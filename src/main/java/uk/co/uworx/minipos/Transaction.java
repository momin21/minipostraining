package uk.co.uworx.minipos;


import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

public class Transaction
{
  private List<LineItem> lineItems;

  public Transaction()
  {
    lineItems = new ArrayList<>();
  }

  public void addItem(LineItem lineItem)
  {
    this.lineItems.add(lineItem);
  }

  public double getTotal()
  {
    return lineItems.stream()
            .filter(lineItem -> !lineItem.isVoided())
            .mapToDouble(lineItem -> (lineItem.getPrice() * lineItem.getQuantity()))
            .sum();
  }

  public LineItem voidItem(String code) throws NoSuchElementException
  {
    Optional<LineItem> tobeVoidedItem = this.lineItems.stream().filter(lineItem -> lineItem.getCode().equals(code)
            && !lineItem.isVoided()).findFirst();
    if (tobeVoidedItem.isEmpty())
    {
      throw new NoSuchElementException("No Item Found with Specific code :" + code);
    }
    else
    {
      LineItem foundLineItem = tobeVoidedItem.get();
      foundLineItem.voidItem();
      return foundLineItem;
    }
  }

  public void updateLineItemQuantity(String code, int quantity) throws NoSuchElementException
  {
    Optional<LineItem> foundLineItem = lineItems.stream().filter(lineItem -> lineItem.getCode().equals(code)).findFirst();
    if (lineItems.isEmpty())
    {
      throw new NoSuchElementException("No item found with specific code: " + code);
    }
    else
    {
      LineItem lineItem = foundLineItem.get();
      lineItem.setQuantity(quantity);
    }
  }

  /**
   * A receipt contains header text ( name of organization )
   * Store Address
   * <p>
   * <p>
   * Item code     Item name          Quantity     Price
   * <p>
   * <p>
   * Total number of Items: Count
   * ---------------------------------------------------
   * Total Balance                                 XXXXX
   * <p>
   * Voided Items will have **VOIDED ITEM** line before printing item details
   * Voided Items are to be printed if asked to print ( by default do not print voided items )
   *
   * @return
   */
  public String getReceipt()
  {
    return lineItems.stream()
            .map(lineItem -> ("Code: " + lineItem.getCode() + " , Name: " + lineItem.getName() +
                                      " , Price: " + (!lineItem.isVoided() ? lineItem.getPrice() : "Voided") + " , Quantity: " +
                                      lineItem.getQuantity()))
            .collect(Collectors.joining("\n"));
  }

  public int getActiveItemsCount()
  {
    return (int) lineItems.stream().filter(lineItem -> !lineItem.isVoided()).count();
  }
}
