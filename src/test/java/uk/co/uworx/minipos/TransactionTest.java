package uk.co.uworx.minipos;

import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TransactionTest
{
  @Test
  public void testTransactionTotalAfterAddingOneItem()
  {
    LineItem lineItem = new LineItem("LI1000", "Smart Watch", 10.5, 1);
    Transaction transaction = new Transaction();
    transaction.addItem(lineItem);
    double total = transaction.getTotal();
    assertEquals(10.5, total, 0);
  }

  @Test
  public void testTransactionTotalAfterAddingMultipleItems()
  {
    LineItem lineItem = new LineItem("LI1000", "Smart Watch", 10.5, 1);
    LineItem lineItem1 = new LineItem("LI1001", "Watch", 5.5, 1);
    LineItem lineItem2 = new LineItem("LI1002", "Smart phone", 35, 1);
    Transaction transaction = new Transaction();
    transaction.addItem(lineItem);
    transaction.addItem(lineItem1);
    transaction.addItem(lineItem2);
    transaction.addItem(lineItem2);
    double total = transaction.getTotal();
    assertEquals(86, total, 1);
  }

  @Test
  public void testTransactionTotalWithNoItems()
  {
    Transaction transaction = new Transaction();
    double total = transaction.getTotal();
    assertEquals(0, total, 1);
  }

  @Test
  public void testTransactionTotalAfterVoidingOneItem()
  {
    LineItem lineItem = new LineItem("LI1000", "Smart Watch", 10.5, 1);
    Transaction transaction = new Transaction();
    transaction.addItem(lineItem);
    LineItem voidedItem = transaction.voidItem("LI1000");

    double total = transaction.getTotal();
    assertEquals(0, total, 0);
    assertTrue(voidedItem.isVoided());
  }

  @Test
  public void testTransactionTotalAfterVoidingNonRegisteredItem()
  {
    LineItem lineItem = new LineItem("LI1000", "Smart Watch", 10.5, 1);
    Transaction transaction = new Transaction();
    transaction.addItem(lineItem);
    String notFoundCode = "LI1001";
    try
    {
      transaction.voidItem(notFoundCode);
    }
    catch (NoSuchElementException error)
    {
      assertEquals("No Item Found with Specific code :" + notFoundCode, error.getMessage());
    }
    double total = transaction.getTotal();
    assertEquals(10.5, total, 0);

  }

  @Test
  public void testTransactionTotalAfterVoidingMultipleItems()
  {
    LineItem lineItem = new LineItem("LI1000", "Smart Watch", 10.5, 1);
    LineItem lineItem1 = new LineItem("LI1001", "Watch", 5.5, 1);
    LineItem lineItem2 = new LineItem("LI1002", "Smart phone", 35, 1);
    LineItem lineItem3 = new LineItem("LI1002", "Smart phone", 35, 1);
    Transaction transaction = new Transaction();
    transaction.addItem(lineItem);
    transaction.addItem(lineItem1);
    transaction.addItem(lineItem2);
    transaction.addItem(lineItem3);

    LineItem voidedItem1 = transaction.voidItem("LI1002");
    LineItem voidedItem2 = transaction.voidItem("LI1002");

    double total = transaction.getTotal();
    assertEquals(16, total, 1);
    assertTrue(voidedItem1.isVoided());
    assertTrue(voidedItem2.isVoided());
    assertNotSame(voidedItem1, voidedItem2);
  }

  @Test
  public void testTransactionTotalAfterVoidingAllItems()
  {
    LineItem lineItem = new LineItem("LI1000", "Smart Watch", 10.5, 1);
    LineItem lineItem1 = new LineItem("LI1001", "Watch", 5.5, 1);
    LineItem lineItem2 = new LineItem("LI1002", "Smart phone", 35, 1);
    LineItem lineItem3 = new LineItem("LI1002", "Smart phone", 35, 1);
    Transaction transaction = new Transaction();
    transaction.addItem(lineItem);
    transaction.addItem(lineItem1);
    transaction.addItem(lineItem2);
    transaction.addItem(lineItem3);

    LineItem voidedItem = transaction.voidItem("LI1000");
    LineItem voidedItem1 = transaction.voidItem("LI1001");
    LineItem voidedItem2 = transaction.voidItem("LI1002");
    LineItem voidedItem3 = transaction.voidItem("LI1002");

    double total = transaction.getTotal();
    assertEquals(0, total, 1);
    assertTrue(voidedItem.isVoided());
    assertTrue(voidedItem1.isVoided());
    assertTrue(voidedItem2.isVoided());
    assertTrue(voidedItem3.isVoided());
  }

  @Test
  public void testDisplayItemsWithNormalItemsInTransaction()
  {
    Transaction transaction = new Transaction();

    transaction.addItem(new LineItem("LI1000", "Smart Watch", 10.5, 1));
    transaction.addItem(new LineItem("LI1001", "Watch", 5.5, 1));
    transaction.addItem(new LineItem("LI1002", "Smart phone", 35, 1));
    transaction.addItem(new LineItem("LI1002", "Smart phone", 35, 1));

    String actualReceipt = transaction.getReceipt();
    String expectedReceipt = "Code: LI1000 , Name: Smart Watch , Price: 10.5 , Quantity: 1\n" +
            "Code: LI1001 , Name: Watch , Price: 5.5 , Quantity: 1\n" +
            "Code: LI1002 , Name: Smart phone , Price: 35.0 , Quantity: 1\n" +
            "Code: LI1002 , Name: Smart phone , Price: 35.0 , Quantity: 1";

    assertEquals(expectedReceipt, (actualReceipt));

  }

  @Test
  public void testTotalOnReceiptOfItemInTransaction()
  {
    fail();
  }

  @Test
  public void testDisplayItemsWithVoidedItemsInTransaction()
  {

    Fixture fixture = new Fixture();
    fixture.givenAnItemIsAddedIntoTheTransaction("LI1000", "Smart Watch", 10.5, 1);
    fixture.givenAnItemIsAddedIntoTheTransaction("LI1001", "Watch", 5.5, 1);
    fixture.givenAnItemIsAddedIntoTheTransaction("LI1002", "Smart phone", 35, 1);
    fixture.givenAnItemIsAddedIntoTheTransaction("LI1002", "Smart phone", 35, 1);

    fixture.givenAnItemIsVoidedInTheTransaction("LI1000");
    fixture.whenTransactionReceiptTextIsObtained();
    fixture.thenVerifyVoidedItemExistsInTheReceipt("LI1000", "**VOIDED ITEM**");
    fixture.thenVerifyActiveItemsCountInTheTransaction(3);

  }

  @Test
  public void testDisplayItemsWithAllVoidedItemsInTransaction()
  {
    LineItem lineItem = new LineItem("LI1000", "Smart Watch", 10.5, 1);
    LineItem lineItem1 = new LineItem("LI1001", "Watch", 5.5, 1);
    LineItem lineItem2 = new LineItem("LI1002", "Smart phone", 35, 1);
    LineItem lineItem3 = new LineItem("LI1002", "Smart phone", 35, 1);
    Transaction transaction = new Transaction();
    transaction.addItem(lineItem);
    transaction.addItem(lineItem1);
    transaction.addItem(lineItem2);
    transaction.addItem(lineItem3);

    transaction.voidItem("LI1000");
    transaction.voidItem("LI1001");
    transaction.voidItem("LI1002");
    transaction.voidItem("LI1002");
    String actualReceipt = transaction.getReceipt();
    String expectedReceipt = "Code: LI1000 , Name: Smart Watch , Price: Voided , Quantity: 1\n" +
            "Code: LI1001 , Name: Watch , Price: Voided , Quantity: 1\n" +
            "Code: LI1002 , Name: Smart phone , Price: Voided , Quantity: 1\n" +
            "Code: LI1002 , Name: Smart phone , Price: Voided , Quantity: 1";

    assertEquals(expectedReceipt, (actualReceipt));
  }

  @Test
  public void testDisplayItemsWithNoItemsInTransaction()
  {
    Transaction transaction = new Transaction();
    String actualReceipt = transaction.getReceipt();

    assertEquals("", (actualReceipt));
  }

  @Test
  public void testNormalLineItemQuantityIncreaseInTransaction()
  {
    LineItem lineItem = new LineItem("LI1000", "Smart Watch", 10.5, 1);
    LineItem lineItem1 = new LineItem("LI1001", "Watch", 5.5, 1);
    Transaction transaction = new Transaction();

    transaction.addItem(lineItem);
    transaction.addItem(lineItem1);

    transaction.updateLineItemQuantity("LI1000", 2);
    double total = transaction.getTotal();
    assertEquals(26.5, total, 1);
  }

  @Test
  public void testNormalLineItemQuantityDecreaseInTransaction()
  {
    LineItem lineItem = new LineItem("LI1000", "Smart Watch", 10.5, 2);
    LineItem lineItem1 = new LineItem("LI1001", "Watch", 5.5, 1);
    Transaction transaction = new Transaction();

    transaction.addItem(lineItem);
    transaction.addItem(lineItem1);

    transaction.updateLineItemQuantity("LI1000", 0);
    double total = transaction.getTotal();
    assertEquals(5.5, total, 1);
  }

  @Test
  public void testNormalLineItemQuantityDecreaseBelowOneInTransaction()
  {
    LineItem lineItem = new LineItem("LI1000", "Smart Watch", 10.5, 2);
    LineItem lineItem1 = new LineItem("LI1001", "Watch", 5.5, 1);
    Transaction transaction = new Transaction();

    transaction.addItem(lineItem);
    transaction.addItem(lineItem1);

    transaction.updateLineItemQuantity("LI1000", -1);
    double total = transaction.getTotal();
    assertEquals(5.5, total, 1);
  }

  private class Fixture
  {
    private Transaction transaction;
    private String receiptText;

    private Fixture()
    {
      transaction = new Transaction();
    }

    private void givenAnItemIsAddedIntoTheTransaction(String code, String name, double price, int quantity)
    {
      transaction.addItem(new LineItem(code, name, price, quantity));
    }

    private void givenAnItemIsVoidedInTheTransaction(String code)
    {
      transaction.voidItem(code);
    }

    private void whenTransactionReceiptTextIsObtained()
    {
      receiptText = transaction.getReceipt();
    }

    private void thenVerifyVoidedItemExistsInTheReceipt(String code, String voidingText)
    {
      fail();
    }

    private void thenVerifyActiveItemsCountInTheTransaction(int expectedCount)
    {
      int actualCount = transaction.getActiveItemsCount();
      assertEquals(expectedCount,actualCount);
    }
  }
}
