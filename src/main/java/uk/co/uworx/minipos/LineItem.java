package uk.co.uworx.minipos;


public class LineItem
{
  private String code;
  private String name;
  private double price;
  private int quantity;
  private boolean voided;


  public LineItem(String code, String name, double price, int quantity)
  {
    this.code = code;
    this.name = name;
    this.price = price;
    this.quantity = quantity;
  }

  public String getName()
  {
    return name;
  }

  public double getPrice()
  {
    return price;
  }

  public int getQuantity()
  {
    return quantity;
  }

  public String getCode()
  {
    return code;
  }

  public void voidItem()
  {
    this.voided = true;
  }

  public boolean isVoided()
  {
    return voided;
  }

  public void setQuantity(int quantity)
  {
    this.quantity = quantity >= 0 ? quantity : 0;
  }
}
